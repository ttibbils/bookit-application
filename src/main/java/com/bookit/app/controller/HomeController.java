package com.bookit.app.controller;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;



@RestController
@RequestMapping(path = "")
public class HomeController {

    @GetMapping("")
    public ResponseEntity<String> getBook() {
        return new ResponseEntity<>("Hello", HttpStatus.OK);
    }
}
